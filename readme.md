Pipeline Helper provided by [https://mod-lab.com/](https://mod-lab.com/)

## Purpose

This packages helps smooth out a few pain points we've experienced in our deployment process while using bitbuckets deployment pipelines. This is not bitbucket exclusive, feel free to use any of these tools in your own deployment pipelines.

## Installation


```shell
composer require modlab/pipeline-helper
```

## Populate Env Helper

Transfer specific environment variables from your build server into an application env var of your specification

### Usage


```shell
php -r 'require("vendor/autoload.php"); \ModLab\PipelineHelper\EnvHelper::populateEnvFile();'
```

### API

#### populateEnvFile
- **Arguments**
    - string `$includePattern`
        - default: `MODL_`
        - usage: Specify which environment variables should be taken from the build service and added to the .env
    - string `$envFile`
        - default: `.env`
        - usage: Location where the helper will place the .env file when it finishes

