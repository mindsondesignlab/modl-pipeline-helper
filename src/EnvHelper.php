<?php

namespace ModLab\PipelineHelper;


class EnvHelper
{
    public static function populateEnvFile(string $includePattern = 'MODL_', string $envFile = '.env')
    {
        $environmentVars = getenv();
        $final = [];

        foreach ($environmentVars as $key => $environmentVar) {
            if (strpos($key, $includePattern) === 0) {
                $final[substr($key, strlen($includePattern))] = $environmentVar;
            }
        }

        $str = "";

        foreach ($final as $k => $v) {
            $str .= "$k=\"$v\"\n";
        }

        $fp = fopen($envFile, 'wb');
        fwrite($fp, $str);
        fclose($fp);
    }
}